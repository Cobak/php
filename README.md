## PHP images repository

Docker Hub: 
  - [Prod](https://hub.docker.com/r/cobak/php-dev)

### Flavors

* cobak/php-dev:7.0
* cobak/php-dev:7.0-dev
* cobak/php-dev:7.1-alpine
* cobak/php-dev:7.1-dev-alpine


### 7.0

* PHP CLI
* PHP FPM daemon listening on port 9000, managed with [Tini](https://github.com/krallin/tini)
* Composer


### 7.0-dev

* All of the above
* plus utilities (nano for editing files, telnet for connectivity testing)
* xdebug enabled, with shell aliases to turn it on and off (xdebon, xdeboff)
* OpenSSH server listening on port 22, accessible with user ```cobak``` and password ```cobak``` (for debugging from PHPStorm)
